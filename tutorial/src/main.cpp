#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

// OpenGL extension wrangler to determine at runtime which OpenGL extensions are supported on a platform.
#include <GL/glew.h>

// Simple OpenGL cross-platform window/context/surface/input library
// You can think of this as a lightweight replacement for SDL
//  Maybe it is to SDL what flask --serve is to nginx ?
#include <GLFW/glfw3.h>
GLFWwindow* window;

// OpenGL mathematics library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <common/shader.hpp>
#include "world_state.hpp"
#include "object.hpp"
#include "tetrahedron.hpp"
#include "floor.hpp"
#include "scene.hpp"
#include "serpenski.hpp"

#define PI 3.14159265


void fatal_error(bool condition, const char* msg, int error_code) {
  if (condition) {
    fprintf(stderr, msg);
    getchar();
    glfwTerminate();
    exit(error_code);
  }
}

int main(void) {
  bool has_error = false;

  // Initialize GLFW and ensure it succeeded
  has_error = !glfwInit();
  fatal_error(has_error, "Failed to initialize GLFW\n", -1);

  // No idea what these do
  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // Create window and ensure that it succeeded
  window = glfwCreateWindow(1024, 768, "OpenGL Tut", NULL, NULL);
  has_error = window == NULL;
  fatal_error(has_error, "Failed to open GLFW window\n", -1);
  glfwMakeContextCurrent(window);

  // Initialize GLEW - doesnt need this yet apparently.
  has_error = glewInit() != GLEW_OK;
  fatal_error(has_error, "Failed to initialize GLEW\n", -1);

  // Ensure we can capture the escape key being pressed - compatibility thing I think
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  // Set the background color
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  // Enable depth test
  glEnable(GL_DEPTH_TEST);
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc(GL_LESS);

  // Setup GLSL shaders
  GLuint programID = LoadShaders(
    "/home/sam/workspace/opengl-cpp-tuts/tutorial/src/simple.vert",
    "/home/sam/workspace/opengl-cpp-tuts/tutorial/src/simple.frag"
  );

  // Get a handle for our "MVP" uniform
  GLuint modelID = glGetUniformLocation(programID, "model");
  GLuint viewID = glGetUniformLocation(programID, "view");
  GLuint projectionID = glGetUniformLocation(programID, "projection");

  // Initialize world state
  WorldState *worldState = new WorldState(programID, window);
  worldState->set_start_position(0.0f, 5.0f, 0.0f);
  worldState->set_fov(glm::radians(90.0f));

  Scene *scene = serpenski_gen_scene(modelID);

  // Scene *scene = new Scene(modelID);
  // scene->add(new Floor(glm::vec3( 0.0f, -0.5f,  0.0f), 0.0f));
  // scene->add(new Tetrahedron(glm::vec3( 4.0f, 0.0f,  4.0f), 0.0f));
  // scene->add(new Tetrahedron(glm::vec3( 4.0f, 0.0f, -4.0f), 0.0f));
  // scene->add(new Tetrahedron(glm::vec3(-4.0f, 0.0f,  4.0f), 0.0f));
  // scene->add(new Tetrahedron(glm::vec3(-4.0f, 0.0f, -4.0f), 0.0f));
  // scene->objects[3]->set_scale(2.0f);

  GLuint VAO, vertex_buffer, color_buffer;
  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  glGenBuffers(1, &vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(scene->g_vertex_buffer_data), scene->g_vertex_buffer_data, GL_STATIC_DRAW);

  glGenBuffers(1, &color_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(scene->g_color_buffer_data), scene->g_color_buffer_data, GL_STATIC_DRAW);

  // Position attribute
  glVertexAttribPointer(
    0,          // match the definition in shader
    3,          // size of data
    GL_FLOAT,   // type of data
    GL_FALSE,   // no idea ?
    0,          // stride: memory distance to consecutive attributes
    (void*) 0   // start:  memory offset to first attribute value
  );
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(
    1,          // match the definition in shader
    3,          // size of data
    GL_FLOAT,   // type of data
    GL_FALSE,   // no idea ?
    0,          // stride: memory distance to consecutive attribute
    (void*) 0   // start:  memoryoffset to first attribute value
  );
  glEnableVertexAttribArray(1);

  float time = 0.0;

  // Keep window open until escape is pressed or window is closed.
  while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0) {

    // Clear the screen.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Use our shader
    glUseProgram(programID);

    // Set mvp matrix based on user input
    worldState->send_state();

    // Buffer vertices (why do we do this with vertex data but not colors?)
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer(
      0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0
    );
    glEnableVertexAttribArray(1);

    scene->draw();

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

    glfwSwapBuffers(window);
    glfwPollEvents();

    time += 0.01;
  }

  // Cleanup the vertex buffer object
  glDeleteBuffers(1, &vertex_buffer);
  glDeleteBuffers(1, &color_buffer);
  glDeleteVertexArrays(1, &VAO);
  glDeleteProgram(programID);

  glfwTerminate();
  return 0;
}
