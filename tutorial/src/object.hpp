#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <vector>

// OpenGL extension wrangler to determine at runtime which OpenGL extensions are supported on a platform.
#include <GL/glew.h>

// Simple OpenGL cross-platform window/context/surface/input library
// You can think of this as a lightweight replacement for SDL
//  Maybe it is to SDL what flask --serve is to nginx ?
#include <GLFW/glfw3.h>

// OpenGL mathematics library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

class Object {
protected:
  int buffer_offset;

public:
  GLfloat scale;
  glm::vec3 position;
  GLfloat orientation;

  Object(glm::vec3 position, GLfloat orientation, GLfloat scale) {
    this->position = position;
    this->orientation = orientation;
    this->scale = scale;
  }

  void set_scale(GLfloat scale) {
    this->scale = scale;
  }

  virtual int get_num_vertices() { return 0; }

  virtual GLfloat* get_vertex_data() { return (GLfloat*) NULL; }

  virtual GLfloat* get_color_data() { return (GLfloat*) NULL; }

  void draw(GLuint modelID) {
    glm::mat4 model = this->get_model_matrix();
    glUniformMatrix4fv(modelID, 1, GL_FALSE, &model[0][0]);
    int num_vertices = this->get_num_vertices();
    glDrawArrays(GL_TRIANGLES, this->buffer_offset, this->buffer_offset + this->get_num_vertices());
  }

  int write_vertex_data(
      int vertex_pos,
      GLfloat vertex_data_buffer[],
      GLfloat color_data_buffer[]) {
    this->buffer_offset = vertex_pos;
    GLfloat* vertex_data = this->get_vertex_data();
    GLfloat* color_data = this->get_color_data();
    for (int i=0; i<this->get_num_vertices(); i++) {
      for (int j=0; j<3; j++) {
        vertex_data_buffer[3 * (i + vertex_pos) + j] = vertex_data[3*i + j];
        color_data_buffer[3 * (i + vertex_pos) + j] = color_data[3*i + j];
      }
    }
    return this->get_num_vertices();
  }

  glm::mat4 get_model_matrix() {
    static glm::vec3 yAxis = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, this->position);
    model = glm::scale(model, glm::vec3(this->scale, this->scale, this->scale));
    model = glm::rotate(model, this->orientation, yAxis);

    return model;
  }
};

#endif
