#include "scene.hpp"
#include "object.hpp"
#include "tetrahedron.hpp"
#include "serpenski.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

void _serpenski_gen_scene(Scene *s, glm::vec4 offset, int depth, int limit) {
  float factor = 1.0f / pow(2.0f, depth - 1);
  for (int i=0; i<4; i++) {
    glm::vec4 new_offset = offset + factor * Tetrahedron::points[i];

    if (depth == limit) {
      s->add(new Tetrahedron(new_offset, 0.0f, factor));
    } else {
      _serpenski_gen_scene(s, new_offset, depth + 1, limit);
    }
  }
}

Scene* serpenski_gen_scene(GLuint modelID) {
  Scene* s = new Scene(modelID);
  _serpenski_gen_scene(s, glm::vec4(0.0f, 0.0f, 0.0f, 0.0f), 1, 3);
  return s;
}
