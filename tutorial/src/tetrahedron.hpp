#ifndef TETRAHEDRON_HPP
#define TETRAHEDRON_HPP

// OpenGL extension wrangler to determine at runtime which OpenGL extensions are supported on a platform.
#include <GL/glew.h>

// Simple OpenGL cross-platform window/context/surface/input library
// You can think of this as a lightweight replacement for SDL
//  Maybe it is to SDL what flask --serve is to nginx ?
#include <GLFW/glfw3.h>

// OpenGL mathematics library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include "object.hpp"

class Tetrahedron : public Object {
private:
  static GLfloat vertex_data[];
  static GLfloat color_data[];

public:
  static glm::vec4 points[];

  Tetrahedron(glm::vec3 position, GLfloat orientation, GLfloat scale) : Object{position, orientation, scale} {}

  int get_num_vertices() {
    return 12;
  }

  GLfloat* get_vertex_data();

  GLfloat* get_color_data();

};

#endif