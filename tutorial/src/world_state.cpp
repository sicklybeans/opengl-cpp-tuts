#include <stdio.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace std;

#include "world_state.hpp"

WorldState::WorldState(GLuint programID, GLFWwindow* window) {
  this->programID = programID;
  this->position = glm::vec3(5, 0, 5);
  this->horizontalAngle = 5 * 3.14f/4.0f;
  this->verticalAngle = 0.0f;
  this->fov = glm::radians(45.0f);
  this->speed = 3.0f;
  this->mouseSpeed = 0.005f;
  this->window = window;
  this->projectionMatrix = glm::perspective(this->fov, 4.0f / 3.0f, 0.1f, 100.0f);
  this->viewID = glGetUniformLocation(programID, "view");
  this->projectionID = glGetUniformLocation(programID, "projection");
}

void WorldState::set_start_position(GLfloat x, GLfloat y, GLfloat z) {
  this->position = glm::vec3(x, y, z);
}

void WorldState::set_fov(GLfloat fov) {
  this->fov = fov;
  this->projectionMatrix = glm::perspective(this->fov, 4.0f / 3.0f, 0.1f, 100.0f);
}

void WorldState::send_state() {
  // glfwGetTime is called only once, the first time this function is called
  static double lastTime = glfwGetTime();
  // Time values
  double currentTime;
  float deltaTime;
  // Mouse positions
  double xpos, ypos;
  // Vectors representing up, right, and forward
  glm::vec3 right, up;
  glm::mat4 viewMatrix;

  currentTime = glfwGetTime();
  deltaTime = float(currentTime - lastTime);

  // Get mouse position
  glfwGetCursorPos(this->window, &xpos, &ypos);

  // Reset mouse position for next frame
  glfwSetCursorPos(this->window, 1024/2, 768/2);

  // Compute new orientation
  this->horizontalAngle += this->mouseSpeed * float(1024/2 - xpos);
  this->verticalAngle   += this->mouseSpeed * float( 768/2 - ypos);

  // Direction : Spherical coordinates to Cartesian coordinates conversion
  glm::vec3 direc(
    cos(this->verticalAngle) * sin(this->horizontalAngle),
    sin(this->verticalAngle),
    cos(this->verticalAngle) * cos(this->horizontalAngle)
  );

  // Right vector
  right = glm::vec3(
    sin(this->horizontalAngle - 3.14f/2.0f),
    0,
    cos(this->horizontalAngle - 3.14f/2.0f)
  );

  // Up vector
  up = glm::cross( right, direc );

  // Determine movement direction
  // Move forward
  if (glfwGetKey( this->window, GLFW_KEY_UP ) == GLFW_PRESS){
    this->position += direc * deltaTime * this->speed;
  }
  // Move backward
  if (glfwGetKey( this->window, GLFW_KEY_DOWN ) == GLFW_PRESS){
    this->position -= direc * deltaTime * this->speed;
  }
  // Strafe right
  if (glfwGetKey( this->window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
    this->position += right * deltaTime * this->speed;
  }
  // Strafe left
  if (glfwGetKey( this->window, GLFW_KEY_LEFT ) == GLFW_PRESS){
    this->position -= right * deltaTime * this->speed;
  }

  glm::vec3 temp = this->position + direc;

  viewMatrix = glm::lookAt(
    this->position,          // Camera is here
    this->position+direc,    // and looks here : at the same position, plus "direction"
    up                  // Head is up (set to 0,-1,0 to look upside-down)
  );

  lastTime = currentTime;

  glUniformMatrix4fv(this->viewID, 1, GL_FALSE, &viewMatrix[0][0]);
  glUniformMatrix4fv(this->projectionID, 1, GL_FALSE, &this->projectionMatrix[0][0]);
}





